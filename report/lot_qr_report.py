from trytond.report import Report


class LotQRReport(Report):
    __name__ = 'stock.lot.qr_report'

    @classmethod
    def get_context(cls, records, data):
        context = super(LotQRReport, cls).get_context(records,data)

        return context
