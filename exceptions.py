# This file is part stock_anmat_traceability module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.exceptions import UserError


class MissingModule(UserError):
    pass


class MissingWSConfiguration(UserError):
    pass


class NotConfirmedTransactionsError(UserError):
    pass


class QtyOutOfRange(UserError):
    pass


class NotEventDefined(UserError):
    pass


class ServiceUnavaible(UserError):
    pass


class AlertTransactionsError(UserError):
    pass


class NumberAlreadyLoaded(UserError):
    pass


class ProductNotFound(UserError):
    pass


class NumberAlreadyPicked(UserError):
    pass
