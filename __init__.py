#  -*- coding: utf-8 -*-
# This file is part stock_anmat_traceability module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.pool import Pool
from . import anmat
from . import party
from . import product
from . import lot
from . import stock

from .report import lot_qr_report

from .wizard import not_confirmed_transactions
from .wizard import cancel_transactions
from .wizard import shipment_in_load_units
from .wizard import shipment_out_pick_units
from .wizard import shipment_in_confirm
from .wizard import shipment_out_inform


def register():
    Pool.register(
        anmat.AnmatConfig,
        anmat.AgentType,
        anmat.EventType,
        anmat.Event,
        anmat.AnmatLocationConfig,
        anmat.AnmatTraceTransaction,
        party.Party,
        party.Address,
        product.Template,
        product.Product,
        stock.Unit,
        stock.Move,
        stock.ShipmentIn,
        stock.ShipmentOut,
        stock.ShipmentInReturn,
        stock.ShipmentOutReturn,
        stock.ShipmentInternal,
        stock.Period,
        stock.PeriodCacheUnit,
        stock.Inventory,
        stock.InventoryLine,
        not_confirmed_transactions.NotConfirmedTransactionStart,
        not_confirmed_transactions.NotConfirmedTransactionResultLine,
        not_confirmed_transactions.NotConfirmedTransactionResult,
        not_confirmed_transactions.NotConfirmedTransactionEmpty,
        not_confirmed_transactions.NotConfirmedTransactionAlertResult,
        cancel_transactions.CancelTransactionStart,
        cancel_transactions.CancelTransactionResult,
        shipment_in_load_units.LoadUnitsStart,
        shipment_in_load_units.LoadUnitsStartLine,
        shipment_out_pick_units.PickUnitsStart,
        shipment_out_pick_units.PickUnitsStartLine,
        shipment_in_confirm.ShipmentInConfirmStart,
        shipment_in_confirm.ShipmentInConfirmResult,
        shipment_out_inform.ShipmentOutInformStart,
        shipment_out_inform.ShipmentOutInformResult,
        lot.Lot,
        module='stock_anmat_traceability', type_='model')
    Pool.register(
        lot_qr_report.LotQRReport,
        module='stock_anmat_traceability', type_='report')
    Pool.register(
        not_confirmed_transactions.NotConfirmedTransaction,
        cancel_transactions.CancelTransaction,
        shipment_in_load_units.LoadUnits,
        shipment_out_pick_units.PickUnits,
        shipment_in_confirm.ShipmentInConfirm,
        shipment_out_inform.ShipmentOutInform,
        module='stock_anmat_traceability', type_='wizard')
